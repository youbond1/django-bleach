from django.db import models
from .validators import BleachValidator
from .utils import get_bleach_default_options
from bleach import clean


class BleachField(object):
    def __init__(self, allowed_tags=None, allowed_attributes=None,
                 allowed_styles=None, strip_tags=None, strip_comments=None,
                 *args, **kwargs):

        super(BleachField, self).__init__(*args, **kwargs)

        self.bleach_kwargs = get_bleach_default_options()

        if allowed_tags:
            self.bleach_kwargs['tags'] = allowed_tags
        if allowed_attributes:
            self.bleach_kwargs['attributes'] = allowed_attributes
        if allowed_styles:
            self.bleach_kwargs['styles'] = allowed_styles
        if strip_tags:
            self.bleach_kwargs['strip'] = strip_tags
        if strip_comments:
            self.bleach_kwargs['strip_comments'] = strip_comments

        self.validators.append(BleachValidator(bleach_kwargs=self.bleach_kwargs))

    def pre_save(self, model_instance, add):
        return clean(
            getattr(model_instance, self.attname),
            **self.bleach_kwargs
        )


class BleachCharField(BleachField, models.CharField):
    pass


class BleachTextField(BleachField, models.TextField):
    pass
