from rest_framework.serializers import CharField
from bleach import clean
from .validators import BleachValidator
from .utils import get_bleach_default_options


class BleachCharField(CharField):
    def __init__(self, *args, **kwargs):
        self.bleach_options = get_bleach_default_options()
        self.validators.append(BleachValidator(bleach_kwargs=self.bleach_options))
        return super(BleachCharField, self).__init__(*args, **kwargs)

    def to_internal_value(self, data):
        retval = super(BleachCharField, self).to_internal_value(data)
        return clean(retval, **self.bleach_options)
