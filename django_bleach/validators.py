from django.utils.deconstruct import deconstructible
from django.forms import ValidationError
from .utils import get_bleach_default_options
from bleach import clean


@deconstructible
class BleachValidator(object):
    def __init__(self, *args, **kwargs):
        self.bleach_kwargs = kwargs.pop('bleach_kwargs', get_bleach_default_options())
        super(BleachValidator, self).__init__(*args, **kwargs)

    def __call__(self, data):
        bleached_data = clean(data, **self.bleach_kwargs)
        if bleached_data != data:
            raise ValidationError("Bad Input: {}".format(data))

    def __eq__(self, other):
        return isinstance(other, BleachValidator)
